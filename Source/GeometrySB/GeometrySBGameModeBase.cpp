// Copyright Epic Games, Inc. All Rights Reserved.


#include "GeometrySBGameModeBase.h"
#include "SandboxPawn.h"
#inckude "SandboxPlayerController.h"

AGeometrySBGameModeBase::AGeometrySBGameModeBase()
{
	// Set base Pawn class:
	DefaultPawnClass = ASandboxPawn::StaticClass();
	
	// Set base Player controller class:
	PlayerControllerClass = ASandboxPlayerController::StaticClass();
}
