// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseGeometryActor.h"
#include "GeometryHubActor.generated.h"

/**
*	 This actor spawning actors Geometry:
**//

/** Data geometry: */
USTRUCT(BlueprintType)
struct FGeometryPayLoad
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
		TSubclassOf<ABaseGeometryActor> GeometryClass;
	UPROPERTY(EditAnywhere)
		FGeometryData Data;
	UPROPERTY(EditAnywhere)
		FTransform InitialTransform;
};

UCLASS()
class GEOMETRYSB_API AGeometryHubActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGeometryHubActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TArray<FGeometryPayLoad> GeometryPayLoads;

	UPROPERTY(EditAnywhere)
		ABaseGeometryActor* GeometryObject;
		//TSubclassOf<AActor> ActorClass;
	UWorld* World = nullptr;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnColorChanged(const FLinearColor& Color, const FString& Name);
	//UFUNCTION()
	void OnTimerFinished(AActor* Actor);

private:
	void DoActorSpawn1(); // Test
	void DoActorSpawn2(); // Test
	void DoActorSpawn3(); // Test
};
