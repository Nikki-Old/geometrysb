// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "BaseGeometryActor.generated.h"

/** Create a delegate to call when the color changes: */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnColorChanged, const FLinearColor&, Color, const FString&, Name );
/** Create a delegate to call when timer finished: */
DECLARE_MULTICAST_DELEGATE_OneParam(FOnTimerFinished, AActor*)

/** Movement status: */
UENUM(BlueprintType)
enum class EMovementType : uint8
{
	Sin, // Sinusoid
	Static
};

/** Stores settings Geometry: */
USTRUCT(BlueprintType)
struct FGeometryData
{
	GENERATED_USTRUCT_BODY()

	/** Movement status: */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementType MoveType = EMovementType::Static;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta (EditConditon = "MoreType != EMovementType::Static"))
		float Amplitude = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta (EditConditon = "MoreType != EMovementType::Static"))
		float Frequency = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Design")
		FLinearColor Color = FLinearColor::Black;
		/** Call frequency: */ // ????
	UPROPERTY(EditAnywhere, Category = "Design")
		float TimerRate = 3.0f;
};

UCLASS()
class GEOMETRYSB_API ABaseGeometryActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseGeometryActor();

	UPROPERTY(VisibleAnyWhere)
		UStaticMeshComponent* BaseMesh;

	// Set Data:
	void SetGeometryData(const FGeometryData& Data) { GeometryData = Data; }
	// Get Data:
	UFUNCTION(BlueprintCallable)
	FGeometryData GetGeometryData() const { return GeometryData; }

	/** Create a delegate to call when the color changes:*/
	UPROPERTY(BlueprintAssignable) //... and can call in Blueprints:
	FOnColorChanged OnColorChanged;
	
	/** Create a delegate to call when timer finished: */
	FOnTimerFinished OnTimerFinished;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GeometryData")
		FGeometryData GeometryData;

	UPROPERTY(EditAnyWhere) // test
		int32 WeaponsNum = 4;

	UPROPERTY(EditDefaultsOnly) // test
		int32 KillsNum = 7;

	UPROPERTY(EditInstanceOnly) // test
		float Health = 32.532234f;

	UPROPERTY(EditAnyWhere) // test
		bool IsDead = false;

	UPROPERTY(VisibleAnyWhere) // test
		bool HasWeapon = true;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	
	FVector InitialLocation; // buffer location.
	FTimerHandle TimerHandle; // Timer for swap color.
	
	const int32 MaxTimerCount = 5;
	int TimerCount = 0;

	void printTypes(); // test

	void printStringTypes(); // test

	void printTransform(); // test

	void MovementByAmplitude(); 

	void SetColor(const FLinearColor& Col);

	void OnTimerFired();
};
