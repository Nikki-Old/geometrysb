// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SandboxPawn.generated.h"

class UCameraComponent;

UCLASS()
class GEOMETRYSB_API ASandboxPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASandboxPawn();

	UPROPERTY(VisibleAnyWhere)
		USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnyWhere)
		UStaticMeshComponent* SMComponent = nullptr;
	UPROPERTY(VisibleAnyWhere)
		UCameraComponent* CameraComponent = nullptr;

	UPROPERTY(EditAnywhere)
		float Velocity = 300.f;

	// Called when this Pawn is possessed
	virtual void ProssessedBy(AController* NewController) override;
	// Called when our Controller no longer possesses us.
	virtual void UnPossessed() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	FVector VelocityVector = FVector::ZeroVector;

	void MoveForward(float Amount);
	void MoveRight(float Amount);
};
