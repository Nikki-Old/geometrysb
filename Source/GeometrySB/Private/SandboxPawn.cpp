// Fill out your copyright notice in the Description page of Project Settings.


#include "SandboxPawn.h"
#include "Components/InputComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Camera/CameraComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogSanboxPawn, All, All)

// Sets default values
ASandboxPawn::ASandboxPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("Root");
	SetRootComponent(SceneComponent);

	SMComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	SMComponent->SetupAttachment(SceneComponent/* Or GetRootComponent() */);

	CameraComponent = CreadefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ASandboxPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASandboxPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!VelocityVector.IsZero())
	{
		const FVector NewLocation = GetActorLocation() + Velocity * DeltaTime * VelocityVector;
		
		SetActorLocation(NewLocation);
		VelocityVector = FVector::ZeroVector;
	}
}

// Called to bind functionality to input
void ASandboxPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if(PlayerInputComponent)
	{
		PlayerInputComponent->BindAxis("MoveForward", this, &ASandboxPawn::MoveForward);
		PlayerInputComponent->BindAxis("MoveRight", this, &ASandboxPawn::MoveRight);
	}

}

void ASandboxPawn::MoveForward(float Amount)
{
	UE_LOG(LogSanboxPawn, Display, TEXT("MoveForward: %.0f"), Amount);
	VelocityVector.X = Amount;
}

void ASandboxPawn::MoveRight(float Amount)
{
	UE_LOG(LogSanboxPawn, Display, TEXT("MoveRight: %.0f"), Amount);
	VelocityVector.Y = Amount;
}

void ASandboxPawn::ProssessedBy(AController* NewController)
{
	Super::ProssessedBy(NewController);
	if(!NewController) return;
	UE_LOG(LogSandboxPawn, Display, TEXT("%s possessed %s"), *GetName(), *NewController->GetName());
}

void ASandboxPawn::UnPossessed()
{
	Super::UnPossessed();

	UE_LOG(LogSandboxPawn, Display, TEXT("%s unpossessed"), *GetName());
}

