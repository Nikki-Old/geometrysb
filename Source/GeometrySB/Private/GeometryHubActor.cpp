// Fill out your copyright notice in the Description page of Project Settings.


#include "GeometryHubActor.h"
#include "Engine/World.h"

DEFINE_LOG_CATEGORY_STATIC(LogGeometryHub, All, All)

// Sets default values
AGeometryHubActor::AGeometryHubActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGeometryHubActor::BeginPlay()
{
	Super::BeginPlay();
	
	World = GetWorld();
}

// Called every frame:
void AGeometryHubActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGeometryHubActor::DoActorSpawn1()
{
	if (World)
	{
		for (int i = 0; i < 10; i++)
		{
			const FTransform GeometryTransform = FTransform(FRotator::ZeroRotator, FVector(0.0f, 300.0f * i, 300.f));
			ABaseGeometryActor* Geometry = World->SpawnActor<ABaseGeometryActor>(GeometryPayLoads[i].GeometryClass, GeometryTransform);
			if (Geometry)
			{
				FGeometryData Data;
				Data.MoveType = FMath::RandBool() ? EMovementType::Static : EMovementType::Sin;
				Geometry->SetGeometryData(Data);
			}
		}
	}

}

void AGeometryHubActor::DoActorSpawn2()
{
	if (World)
	{
		for (int i = 0; i < 10; i++)
		{
			const FTransform GeometryTransform = FTransform(FRotator::ZeroRotator, FVector(0.0f, 300.0f * i, 700.f));
			ABaseGeometryActor* Geometry = World->SpawnActorDeferred<ABaseGeometryActor>(GeometryPayLoads[i].GeometryClass, GeometryTransform);
			if (Geometry)
			{
				FGeometryData Data;
				//Data.MoveType = FMath::RandBool() ? EMovementType::Static : EMovementType::Sin;
				Data.Color = FLinearColor::MakeRandomColor();
				Geometry->SetGeometryData(Data);Geometry->FinishSpawning(GeometryTransform);
			}
		}
	}

}

void AGeometryHubActor::DoActorSpawn3()
{
	if (World)
	{
		for (const FGeometryPayLoad PayLoad : GeometryPayLoads)
		{
			ABaseGeometryActor* Geometry = World->SpawnActorDeferred<ABaseGeometryActor>(PayLoad.GeometryClass, PayLoad.InitialTransform);

			if (Geometry)
			{
				Geometry->SetGeometryData(PayLoad.Data);
				Geometry->OnColorChanged.AddDynamic(this, &AGeometryHubActor::OnColorChanged);
				Geometry->OnTimerFinished.AddUObject(this, &AGeometryHubActor::OnTimerFinished);
				Geometry->FinishSpawning(PayLoad.InitialTransform);
			}
		}
	}

}

void AGeometryHubActor::OnColorChanged(const FLinearColor& Color, const FString& Name)
{
	UE_LOG(LogGeometryHub, Display, TEXT("Actor name: %s, changed color %s."), *Name, *Color.ToString());

}

void AGeometryHubActor::OnTimerFinished(AActor* Actor)
{
	if(!Actor) return;
	UE_LOG(LogGeometryHub, Display, TEXT("Actor name: %s, timer - finished."), *Actor->GetName());

	ABaseGeometryActor* Geometry = Cast<ABaseGeometryActor>(Actor);
	if(!Geometry) return;

	UE_LOG(LogGeometryHub, Display, TEXT("Cast is success, amplitude %f ."), Geometry->GetGeometryData().Amplitude);

	Geometry->Destroy(); // Or Geometry->SetLifeSpan(2.f);
}

