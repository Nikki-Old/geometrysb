// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GeometrySBGameModeBase.generated.h"

/**
 *  GameMode base class for this projecte:  
 */

UCLASS()
class GEOMETRYSB_API AGeometrySBGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AGeometrySBGameModeBase();
};
